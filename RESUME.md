# Preston Maness - January 08 2025 

* __Phone__: 512-955-1048 (I'm on [Signal](https://signal.org/))
* __Email/Public-Key__: [aspensmonster@riseup.net](mailto:aspensmonster@riseup.net?subject=About%20Your%20Resume)
    * Fingerprint: 7989 5B2E 0F87 503F 1DDE  80B6 4976 5D7F 0DDD 9BD5
    * Keyoxide (like Linktree): [https://keyoxide.org/hkp/79895B2E0F87503F1DDE80B649765D7F0DDD9BD5](https://keyoxide.org/hkp/79895B2E0F87503F1DDE80B649765D7F0DDD9BD5)
* __Code__: [https://codeberg.org/aspensmonster/](https://codeberg.org/aspensmonster?tab=activity) 
* __StackExchange/SuperUser__: [https://superuser.com/users/220550/preston-maness](https://superuser.com/users/220550/preston-maness)
* __LinkedIn__: [http://www.linkedin.com/pub/preston-maness/8b/973/202](http://www.linkedin.com/pub/preston-maness/8b/973/202)

# Experience, Certifications, and Education

* Employment
    * __WhAtS WiTh ThE GaP iN yOuR ReSUmE?__ (October 2022 - Present): Chronic
      illness is a bitch :) I suffer from Chronic Pancreatitis. It tends to put
me in the hospital at least twice a year for at least a week at a time, and
likes to also flare up inbetween those visits too. I *have* had some
intermittent employment between October 2022 and now, but, sUrPRiSiNgLY,
employers don't like it when their employees are sick "too much." Maybe you'll
be the exception?
    * __Viatech__ (10 months ; December 2021 - September 2022): __Backend
      Software Engineer__, maintain and develop multiple microservices written
in dotnet core and deployed via Azure DevOps to a managed Kubernetes cluster.
Services used included Azure Functions (including Durable Functions), Azure
DevOps, Azure App Configuration, Azure Service Bus, Azure Container Registry,
and Azure Application Insights.
        * Developed an email notification system based off of Razor templates.
        * Created and maintained a mointoring dashboard with App Insights.
        * Migrated several microservices from dotnet core 3.1 to 6 (also
	  entailed Azure Functions updates).
        * Design, build, test, maintain API endpoints according to product
          needs, making use of design patterns like Repositories and Providers,
and libraries like Entity Framework.
    * __360Connect__ (3 years, 5 months ; Apr. 2018 - November 2021):
      __Software Engineer__, maintain legacy C# codebases, develop new ones,
Typescript Angular front-end for three internal portals.
        * Bootstrapped an Angular 12 customer portal with premium PrimeNG
          theme, train junior hires on Angular framework principles, guide and
review development of the portal. This portal should replace legacy portals by
2022Q1 and provide more real-time feedback to our customers on their
performance. The intended goals are increased customer satisfaction and
retention.
        * Replaced a manual Mystery Shop program for our customers with an
          automated one that utilized a third-party service to programmatically
obtain tracking phone numbers so that our customers could measure their sales
teams' performance with leads we sent them (how often were buyers called, how
quickly). Early response from customers is mostly enthusiastic, and the hope is
that we cut churn in half with this new service.
        * Implemented some Lightning Web Components and VisualForce pages in our
SalesForce Org in order to assist Account Management in syncing specific data
between SalesForce and our own internal systems.
        * Introduced automated transactional emails (mailgun, MJML) with
          trackable phone numbers and trackable links for measuring engagement
between buyers and suppliers regarding suppliers' services, attempting to
encourage buyers to contact sellers directly (this has resulted in improved ROI
for some customers and in turn improved retention/reduced churn).
        * Built ASP.NET Web API 2 back-end for new Angular front-ends. Balance
          speed of releases with management of technical debt. Aim to follow
SOLID principles. Manage CI infrastructure and deployment of application to
Azure App Services. (Incomplete list of involved libraries, frameworks, etc:
SimpleInjector for DI/IOC, FluentValidation for initial payload validating,
Entity Framework for database interaction, HangFire for off-pipeline jobs,
ASP.NET Identity for user management, NUnit for unit and integration testing)
        * Re-built an internal tool for performing automated, near real-time
	  bidding on SEM platforms (AdWords, Bing) that reflected our current
inventory. Resulted in improved margin/ROAS.

    * __360Connect__ (2 years ; Apr. 2016 - Mar. 2018): __Software Support
      Engineer__, involved in managing systems integrations and analytics
pipelines (managing middleware and certain infrastructure).
        * Built and maintained [Snowplow
	  Analytics](http://snowplowanalytics.com/) real-time and batch
pipeline on AWS, utilizing Ansible to manage the infrastructure as code. This
pipeline provides the organization with thorough event-level data on all
integrated systems and online properties, enabling the organization to perform
more thorough and extensible business analysis at a fraction of the cost of
alternatives (Google, Adobe, et.al. offerings).
        * Provided feature enhancements to Coffeescript plugins (tested with
	  Mocha/Chai) for the
[LeadConduit](http://activeprospect.com/products/leadconduit/) platform:
            * [extending XML integration](https://github.com/activeprospect/leadconduit-integration-custom/issues/26)
            * [extending JSON integration](https://github.com/activeprospect/leadconduit-integration-custom/pull/28)
            * [extending response handlers](https://github.com/activeprospect/leadconduit-integration-custom/pull/43)
        * Made tweaks to a legacy C# codebase in order to capture analytics
	  data that was previously not collected.
        * Automated numerous aspects of position by utilizing public APIs and
	  python:
            * Wrote python script to [appropriately
	      poll](https://documentation.mailgun.com/api-events.html#event-polling)
mailgun event API and upload to S3 for further ETL work and alerting.
            * Used python extensively to interact with [LeadConduit Platform
	      API](http://docs.activeprospect.com/leadconduit/resources.html)
and programmatically manage/update/deploy lead flows. Also backed up daily data
to S3.
            * Wrote ETL scripts in python to marshal and munge data from S3
	      into Redshift, for later joining with other business
intelligence.
    * __Rackspace Inc.__ (11 months ; Oct. 2014 - Sep. 2015): __GNU/Linux Systems
      Administrator I__. Everything I did at HostGator and more :)
Customer-facing support via calls and tickets. Position requires understanding
of Cisco and F5 firewalls, load balancers, and switches, along with a
high-level understanding of datacenter network architecture (aggrs, core
routers, edge routers, multiple back-end management networks). Work with DAS,
NAS, and SAN storage mechanisms.  Basic VMware tasks (clone, resize, vMotion,
etc). Support and troubleshoot our OpenStack-based cloud products. Developed
internal python tool that interacts with several internal REST APIs to automate
numerous issues where networking and systems administration teams intersect in
an effort to reduce response times to incidents even further.
    * __HostGator.com LLC__ (13 months ; Jan. 2011 - Feb. 2012): GNU/Linux Web
      Hosting Systems Administration (Calls, Chats, Email), understanding of
Apache, Exim, Dovecot, MySQL, cPanel, WHM, bash/perl scripting, strace
debugging, customer site performance evaluation and optimization. Pushed for
more thorough, root-cause analysis of customer issues at __first contact__ to
avoid unnecessarily prolonging service degradation.

* Certifications and Continuing Education
    * I have read [The Rust Programming
      Language](https://doc.rust-lang.org/book/) book cover-to-cover.
    * Pluralsight [profile](https://app.pluralsight.com/profile/preston-maness)
    * Build WebApps with Blazor, May 2022, [Manning Publications
      Co.](https://s3-us-west-2.amazonaws.com/lx-common-resources/certificates/u524117/s2635-U8f97093b-a1cd-47cb-9dcf-98b57d97abfb/cert.pdf)
    * Microsoft Excel Beginner to Advanced, April 2022,
      [Udemy](https://www.udemy.com/certificate/UC-ab7d80fa-1761-4eae-aa25-f440171d6bc8/)
    * (Expired) Red Hat Certified System Administrator, RHEL 7,
      [150-035-228](https://www.redhat.com/wapps/training/certification/verify.html?certNumber=150-035-228&isSearch=False&verify=Verify)

* Education
    * Bachelor of Science, Electrical Engineering, Computer Engineering
      concentration, Texas State University, class of 2014
    * Associate of Science, Applied Mathematics, Texas State University, class
      of 2014
    * In Progress: Associates of Science, Nursing, Tarrant County College.

# The Involvement

* Community involvement
    * Member of the Party for Socialism and Liberation since 2022. Heavily
      involved in an effort to struggle against rate hikes at Austin Energy
that would have rewarded wasteful energy users with lower bills, while stiffing
working class Austinites with higher bills -- up to 80 percent higher -- for
conserving energy. This work entailed...
      * Giving public testimony at Austin City Council against the rate hike.
      * Canvassing working class neighborhoods to speak to Austinites in English
and Spanish about the upcoming rate hike.
      * Organizing with coalition members like the CPUSA to flyer working class
neighborhoods to raise awareness for the rate hike.
      * Agitating on social media to raise awareness and fight the rate hike
        with testimony, delivered to city council, from ordinary working-class
Austinites.
      * Speaking to local media about our efforts against the rate hike.
    * Member of the Austin DSA from 2020 to 2022. Heavily involved in a ballot
      measure election effort in 2021 (against Proposition B in May 2021, which
criminalized homelessness and unfortunately passed despite our efforts)
      * Managed [Spoke](https://github.com/MoveOnOrg/Spoke) software (mass
        peer-to-peer texting software hosted on Heroku and backed by Twilio and
Auth0). Ensured texters were able to contact 50,000 Austinites in the lead-up
to the election regarding an anti-homeless ordinance that was on the ballot.
Also wrote custom code to extend this software in connection with our
efforts(see "Participation in Open Source and Free Software Community" section)
      * Administered dozens of events on
        [ActionNetwork](https://actionnetwork.org/) for the effort, ensuring
pixel tracking was in place for volunteers and positive IDs so that we could
reach them across multiple mediums. Crafted some mass emails in ActionNetwork
too.
      * Built, admined, and updated
    [noonpropb2021.org](https://web.archive.org/web/20210622021632/https://www.noonpropb2021.org/)
off of a pre-made template, hosted on AWS CloudFront. Involved some custom
Javascript (jQuery).  Close integration with
[ActBlue](https://secure.actblue.com/) for fundraising and
[ActionNetwork](https://actionnetwork.org/) and
[Mobilize](https://www.mobilize.us/) for volunteer recruitment and management.
      * Helped administer social media profiles and requests for donations.
      * Organized a yard-sign drive that involved raising several thousand
        dollars, working with a designer to produce a single-color sign, having
two 500-count print runs of that sign, and organizing a volunteer effort to
distribute hundreds of those signs to dozens of zip codes around Austin.
      * Bottom-lined virtual phonebanks in opposition to anti-homeless
        ordinances that were on the ballot in Austin.
      * Managed Facebook and Google ad campaigns in opposition to anti-homeless
        ordinances that were on the ballot in Austin.
      * Integrated multiple disparate data sources into a data warehouse to
perform positive ID analysis via SQL.
    * Volunteered as a Bernie Victory Captain (2019-2020): 20+ hours per week
      of organizing, recruiting for, and bottom-lining textbanks, phonebanks,
and neighborhood canvasses during the 2020 Democratic Party primary race in
support of Bernie Sanders' bid for the Democratic Party nomination for the
presidency in 2020.
* Participation in Open Source and Free Software Community
    * Currently (2023-06-19) contributing to the
      [Keyoxide](https://keyoxide.org/) project.  Several small PRs provided:
      * Make URL scheme configurable in library, [PR
        48](https://codeberg.org/keyoxide/doipjs/pulls/48)
      * Make URL scheme configurable in app, [PR
        164](https://codeberg.org/keyoxide/keyoxide-web/pulls/164)
      * Fix formatting of ActivityPub profile display names, [PR
        49](https://codeberg.org/keyoxide/doipjs/pulls/49)
      * Improve handling of fingerprints that don't precisely conform to
        openpgp4fpr specification, [PR
50](https://codeberg.org/keyoxide/doipjs/pulls/50)
    * [Feature enhancement of
      Spoke](https://github.com/MoveOnOrg/Spoke/pull/1995) (Extended Spoke to
provide ability to push payloads of events that occurred in Spoke to external
systems via HTTP JSON payloads. Used this with the Austin DSA to automatically
update internal election spreadsheet data on Google Docs via Zapier and to
update the team internally via Slack bots.)
    * [Staying on top of bugs my code
      introduces](https://github.com/BOINC/boinc/issues/1530) (My idle
detection code for the BOINC project had a null pointer dereference bug under
certain environment conditions)
    * [Debian GNU/Linux Package
      Maintenance](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=815214)
(Fixed logging problems when moving from sysv init to systemd by tweaking the
service file of the BOINC package.)
    * [Regression squash and feature enhancement for BOINC distributed
      computing project](https://github.com/BOINC/boinc/pull/1453) ( __C++__ ;
A pull request that fixed idle detection regression in BOINC client
--originally reported in a [Debian GNU/Linux
bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=721298) I helped to
troubleshoot-- and improved its handling of various edge cases. This also
introduced me to the autotools build suite/toolchain.)
    * [Bug Squashing with GNU FSF's
      `wget`](https://savannah.gnu.org/bugs/index.php?36570) ( __C__ ; Tracked
down a stubborn segfault that was killing long-running web scrapes prematurely
in a non-recoverable manner)
* Languages and Tools
    * __Programming__: Bash, Python, SQL, Javascript-related, C#, Typescript,
      NodeJS (older experience: C++, C, Java, VHDL, HCS12 assembly)
    * __Tooling__: git, GCC, eclipse, make, Doxygen, jenkins, valgrind, Visual
      Studio, numerous javascript-related tools, Excel
    * __Operations__: 
        * GNU/Linux: Apache, MySQL, PHP, Perl, Varish HTTP accelerator,
	  Wordpress, MediaWiki, Nagios monitoring, pnp4nagios, Vagrant VM
manager, Ansible automation, APT and RPM package management, Debian, CentOS and
RHEL distribution experience, basic LDAP authentication and authorization
mechanisms.
        * Windows: Experience with Azure App Services, Azure SQL
        * AWS: Experience with EC2, LCs, ASGs (and lifecycle hooks), ELBs, S3,
	  VPCs, CloudWatch, DynamoDB, Kinesis streams, Elasticsearch Service,
EMR, Redshift, IAM; all typically managed with an infrastructure-as-code
approach with Ansible. Also utilize the AWS CLIs.
        * Networking: Understanding of: Cisco firewall and CSS configs, F5 load
	  balancer configs, OSI network model, VLANs (I also dabble with
Mikrotik network gear).

